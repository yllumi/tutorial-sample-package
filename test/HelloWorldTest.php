<?php

declare(strict_types=1);

namespace TutorialSamplePackage\Test;

use PHPUnit\Framework\TestCase;
use TutorialSamplePackage\HelloWorld;
use function ob_get_clean;
use function ob_start;

final class HelloWorldTest extends TestCase
{
    public function testShowMessage() : void
    {
        ob_start();
        HelloWorld::message('Good Morning!');
        $content = ob_get_clean();

        $this->assertEquals('Hello world: Good Morning!', $content);
    }
}
